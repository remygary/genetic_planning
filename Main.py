import argparse
import threading
import time
import signal
import sys


from flask import Flask
from flask_restful import Api, Resource, reqparse

from Fitness import Fitness
from Tools import base_tool
from ValuePrint import *
from life_cycle import LifeCycle
from server import server


def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    print_whole_genome(main.best_dna)
    print(f"Computation Done, best fitness: {Fitness(main.best_dna).calc_fitness(main.best_dna).score}")
    import config
    config.thread_pool.terminate()
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)

GENERATION_SIZE = 500
KEEP_FOR_MUTATION = 0
BEST_DNA_COUNT = 0
WORST_DNA_COUNT = 0
HERITAGE_SIZE = 0
MUTATION_BASE_VALUES = 60
PRINT_DEBUG = False


def globally_change(gen_size):
    global GENERATION_SIZE
    global KEEP_FOR_MUTATION
    global BEST_DNA_COUNT
    global WORST_DNA_COUNT
    global HERITAGE_SIZE
    GENERATION_SIZE = gen_size
    KEEP_FOR_MUTATION = int(GENERATION_SIZE / 5)
    BEST_DNA_COUNT = GENERATION_SIZE * (1 / 5.)
    WORST_DNA_COUNT = GENERATION_SIZE * (3 / 5.)
    HERITAGE_SIZE = int((GENERATION_SIZE - WORST_DNA_COUNT) / 2)


class Main:

    def __init__(self):
        self.best_dna = None

    def main(self, generations: int):
        dnas = LifeCycle.mass_generation(GENERATION_SIZE)
        prev_best_fitness = -100000000
        start_time = time.time()
        for generation in range(1, generations):

            dnas = LifeCycle.life_cycle(dnas, generation)
            dnas, best_fitness = Fitness.sort_adn_list_per_fitness(dnas, generation)

            print(f"===================GEN [{generation}]======================")
            # Main.best_dnas = []
            for i in range(0, 15):
                if best_fitness > prev_best_fitness and i == 0:
                    print(f"{base_tool.PREVIOUS_BEST_COLOR}DNA[{i:>2}]: {dnas[i].score: >8} | {str(dnas[i].origin)[13:]: >14} | {str(dnas[i].real_origin)[13:]: <14} | Built at {dnas[i].built_at_gen: >3} | Is inter-day valid {dnas[i].valid_interday} | Is per-day work time valid {dnas[i].valid_worker_time_per_day}{base_tool.NO_COLOR}")
                else:
                    print(f"DNA[{i:>2}]: {dnas[i].score: >8} | {str(dnas[i].origin)[13:]: >14} | {str(dnas[i].real_origin)[13:]: <14} | Built at {dnas[i].built_at_gen: >3} | Is inter-day valid {dnas[i].valid_interday} | Is per-day work time valid {dnas[i].valid_worker_time_per_day}")

            # if generation % 100 == 0 and generation != 0:
            #     print(f"Generation {generation}: --- %s seconds ---" % (time.time() - start_time))
            #     start_time = time.time()
            if best_fitness > prev_best_fitness:
            #     base_tool.print_best_fitness(generation, best_fitness, dnas[0])
                prev_best_fitness = best_fitness
                self.best_dna = dnas[0]
            print("END LOOP") if PRINT_DEBUG else 0
            # print("--- %s seconds ---" % (time.time() - start_time))
        print(f"Computation Done, best fitness: {prev_best_fitness}")
        dnas, best_fitness = Fitness.sort_adn_list_per_fitness(dnas, 0)
        print_whole_genome(dnas[0])
        print(f"Computation Done, best fitness: {Fitness(dnas[0]).calc_fitness(dnas[0]).score}")


if __name__ == '__main__':
    threading.Thread(target=server.flaskThread).start()
    print("running the rest of the app")
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--pool", help="Gene pool size", type=int, default=50)
    parser.add_argument("-g", "--generations", help="Generation count", type=int, default=1000)
    args = parser.parse_args()
    globally_change(args.pool)
    main = Main()
    main.main(args.generations)
