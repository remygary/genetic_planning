from random import randint

from FitnessValues import *


class Worker:
    def __init__(self, name: str, remaining_hours: int):
        self.name = name
        self.remaining_hours = remaining_hours
        self.base_hour_target = remaining_hours

    def copy(self):
        new_worker = Worker(self.name, self.remaining_hours)
        new_worker.base_hour_target = self.base_hour_target
        return new_worker

    def __str__(self):
        return "Workers.Worker object at {}. Name: {} | Remaining hours: {}".format(hex(id(self)), self.name, self.remaining_hours)


class Workers:
    def __init__(self):
        self.workers = [
            Worker('Jean', 39),
            Worker('Francis', 39),
            Worker('Michel', 39),
            Worker('Paul', 39),
            Worker('Alice', 39),
            Worker('Ginette', 39),
            Worker('Gwenaelle', 39),
            Worker('Julie', 25)
        ]
        self.workers_count = len(self.workers)

    def get_worker_per_name(self, name: str):
        for worker in self.workers:
            if worker.name == name:
                return worker
        return None

    def get_random_worker(self) -> Worker:
        return self.workers[randint(0, self.workers_count - 1)]


    def get_best_worker_by_fitness(self, day, hour_idx: int) -> Worker:
        results = []
        for worker in self.workers:
            worker_result = {
                "worker": worker,
                "total_day_hour_count": 0,
                "continuous_hour_for_slot": 0,
                "value": -100000000
            }
            results.append(worker_result)
            if day.worker_is_assigned_to_hour(worker, day.hours[hour_idx]):
                continue
            stop_count_before = False
            i = hour_idx -1
            while i > 0:
                if day.worker_is_assigned_to_hour(worker, day.hours[i]):
                    if not stop_count_before:
                        worker_result["continuous_hour_for_slot"] += 1
                    worker_result["total_day_hour_count"] += 1
                else:
                    stop_count_before = True
                i -= 1
            stop_count_after = False
            for i in range(hour_idx, day.hours_count):
                if day.worker_is_assigned_to_hour(worker, day.hours[i]):
                    if not stop_count_after:
                        worker_result["continuous_hour_for_slot"] += 1
                    worker_result["total_day_hour_count"] += 1
                else:
                    stop_count_after = True
            worker_result["value"] = fitness_continuous_hours_values(worker_result["continuous_hour_for_slot"]) + fitness_total_hours_per_day(worker_result["total_day_hour_count"])
        best_worker = results[0]
        for result in results:
            if best_worker["value"] < result["value"]:
                best_worker = result
        return best_worker["worker"]




    def get_worker_with_most_remaining_hours(self) -> Worker:
        result = None
        for worker in self.workers:
            if worker.remaining_hours == 0:
                continue
            if result is None or result.remaining_hours < worker.remaining_hours:
                result = worker
        return result

    def get_worker_with_most_remaining_hours_except(self, worker_list_exception) -> Worker:
        result = None
        for worker in self.workers:
            if worker in worker_list_exception:
                continue
            if worker.remaining_hours == 0:
                continue
            if result is None or result.remaining_hours < worker.remaining_hours:
                result = worker
        return result

    def copy(self):
        new_workers = Workers.__new__(Workers)
        new_workers.workers = [worker.copy() for worker in self.workers]
        new_workers.workers_count = len(new_workers.workers)
        return new_workers
