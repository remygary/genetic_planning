from unittest import TestCase
from Fitness import Fitness
from Genome.DNA import DNA
from Genome.Days.DayOfWeek import Monday
from Genome.GeneticWeek import GeneticWeek


class TestFitness(TestCase):
    fitness = Fitness(DNA())

    def test_calc_fitness(self):
        self.fail()

    def test_count_overtime_per_week(self):
        self.fail()

    def test_count_undertime_per_week(self):
        self.fail()

    def test_count_avg_rest_on_last_two_days(self):
        self.fail()

    def test_count_avg_continuous_work_time(self):
        self.fail()

    def test_find_empty_worker_slot(self):
        self.fail()

    def test_get_score_per_worker_per_week(self):
        week = GeneticWeek()

        monday = Monday()
        monday.hours = [
            ['Paul', 'Gwenaelle'],
            ['Paul', 'Gwenaelle'],
            ['Paul', 'Gwenaelle', 'Michel'],
            ['Paul', 'Gwenaelle', 'Michel'],
            ['Paul', 'Gwenaelle', 'Michel'],
            ['Paul', 'Gwenaelle', 'Michel'],
            ['Paul', 'Gwenaelle'],
            ['Paul', 'Gwenaelle'],
            ['Francis', 'Alice'],
            ['Francis', 'Julie', 'Michel'],
            ['Francis', 'Julie', 'Michel'],
            ['Francis', 'Julie', 'Michel'],
            ['Francis', 'Julie', 'Michel'],
            ['Francis', 'Julie', 'Michel'],
            ['Francis', 'Julie'],
            ['Francis', 'Julie'],
        ]
        week.days = [monday]
        score = self.fitness.get_score_per_worker_per_week(week, True)
        # Paul = 8 hours = 0 + 50                  score = 50     | 39 - 8      = 31    score = -10000
        # Gwenaelle = 8 hours = 0 + 50             score = 100    | 39 - 8      = 31    score = -10000
        # Francis = 8 hours = 0 + 50               score = 150    | 39 - 8      = 31    score = -10000
        # Julie = 7 hours = 0 + 0                  score = 150    | 39 - 7      = 32    score = -10000
        # Michel = 4 + 5 hours = -100 + -50 + 0    score = 0      | 39 - 4 + 5  = 30    score = -10000
        # Alice = 1 hour = -2600 + -400            score = -3000  | 39 - 1      = 38    score = -10000
        # Jean = 0 hour = 0                        score = 0      | 39 - 0      = 39    score = -10000
        # Ginette = 0 hour = 0                     score = 0      | 39 - 0      = 39    score = -10000
        self.assertEqual(score, -83000, f"needed -3000, got {score}")
