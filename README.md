13/10 Known Issues:

- A worker can be selected multiple times for a day, leading to way more than 8 hours of work
- (fixed) ~~There are not enough workers to fill all hour slots... 8 * 39 = 312 hours, but a week is (8 * 2 + 9 * 3) * 7 = 301... This is an issue...~~
- ~~Needs a fitness to lower a genome value if a worker works for less than 4 hours straight and more than 8 hours during a day~~
- Some fitness added
- The week mechanism might need to be reworked to handle a single continuous time-line of days.
