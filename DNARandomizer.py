from random import Random

from Genome.DNA import DNA
from Tools.genome_origin import GenomeOrigin


class DNARandomizer():
    rand = Random()

    @staticmethod
    def randomize(dna: DNA, change_percentage: int) -> DNA:
        rand = DNARandomizer.rand.randint
        for week in dna.weeks:
            for day in week.days:
                for hour_idx in range(0, day.hours_count):
                    hour = day.hours[hour_idx]
                    for slot in hour:
                        if rand(0, 100) < change_percentage:
                            original_worker = week.workers.get_worker_per_name(slot)
                            if original_worker is not None:
                                original_worker.remaining_hours += 1
                            # worker = week.workers.get_random_worker()
                            worker = week.workers.get_best_worker_by_fitness(day, hour_idx)
                            worker.remaining_hours -= 1
                            slot = worker.name
        dna.origin = GenomeOrigin.MUTATION
        dna.real_origin = GenomeOrigin.MUTATION
        return dna
