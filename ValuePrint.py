from Genome.DNA import DNA


def print_whole_genome(dna: DNA):
    print("DNA: ")
    for week in dna.weeks:
        print("Week: ")
        for day in week.days:
            print("\tDay:")
            for hour in range(0, day.hours_count):
                print(f"\t\t{day.hours[hour]}")


def print_fitness_values(fitname: str, fitvalue: int):
    print(f"{fitname:30}: {fitvalue}")


def print_worker_remaining_hours(dna: DNA):
    print("Worker Remaining Hours: ")
    total_remaining = 0
    for week in dna.weeks:
        for worker in week.workers.workers:
            print(f"\t{worker.name:20}: {worker.remaining_hours}")
            total_remaining += worker.remaining_hours
    print(f"Total remaining hours: {total_remaining:2}")
