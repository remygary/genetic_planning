import numpy

from FitnessValues import *
from Genome.DNA import DNA
from Genome.GeneticDay import GeneticDay
from Genome.GeneticWeek import GeneticWeek
from Tools import base_tool
from Tools.genome_origin import GenomeOrigin

class Fitness:
    percentile = {}
    best_dnas = {}
    generation = 0

    def __init__(self, dna: DNA):
        self.dna = dna
        self.score = None

    @staticmethod
    def calc_fitness(dna: DNA, debug: bool=False):
        dna.score = Fitness.find_empty_worker_slot(adn=dna)
        prev_weeks = 0
        i = 1
        for week in dna.weeks:
            print(f"Getting score for week: {i}") if debug else 0
            dna.score += Fitness.get_score_per_worker_per_week(week, debug)
            print(f"Score for week {i}: {dna.score - prev_weeks}") if debug else 0
            prev_weeks = dna.score
        dna.score += Fitness.count_undertime_per_week(dna)
        if dna.valid_interday:
            dna.score += (20000 * dna.weeks_count)
        if dna.valid_worker_time_per_day:
            dna.score += (500 * dna.weeks_count * dna.weeks[0].days_count)
        return dna

    @staticmethod
    def count_undertime_per_week(dna: DNA):
        count = 0
        for week in dna.weeks:
            # worker_count = 0
            for worker in week.workers.workers:
                # worker_count += 1
                count += (worker.remaining_hours * FITNESS_UNDERHOUR_PER_WEEK)
                # print(f"{worker.name} has {worker_count if worker.remaining_hours > 0 else -worker_count} remaining hours")
        return count

    def count_avg_rest_on_last_two_days(self, week):
        return 0.4

    def count_avg_continuous_work_time(self, day):
        return 7.3

    @staticmethod
    def find_empty_worker_slot(adn: DNA):
        count = 0
        for week in adn.weeks:
            for day in week.days:
                for h in day.hours:
                    for s in h:
                        if s is None:
                            count += FITNESS_EMPTY_WORKER_SLOT
        return count

    @staticmethod
    def get_score_per_worker_per_week(week: GeneticWeek, debug: bool=False):
        score = 0
        for worker in week.workers.workers:
            print(f"\tGetting score for {worker.name}") if debug else 0
            total_week_hour_count = 0
            for day in week.days:
                total_day_hour_count = 0
                continuous_hour_count = 0
                continuous_hour_message = ''
                for hour in day.hours:
                    if GeneticDay.worker_is_assigned_to_hour(worker, hour):
                        continuous_hour_count += 1
                        total_day_hour_count += 1
                    else:
                        if continuous_hour_count != 0 and debug:
                            if continuous_hour_message == '':
                                continuous_hour_message += '' + str(continuous_hour_count)
                            else:
                                continuous_hour_message += ' and ' + str(continuous_hour_count)
                            # print(f"\t{worker.name} worked for {continuous_hour_count} continuous hours on {day.day_idx}.") if debug else 0
                        score += fitness_continuous_hours_values(continuous_hour_count, debug)
                        continuous_hour_count = 0
                # if continuous_hour_count != 0:
                #     print(f"{worker.name} worked for {continuous_hour_count} continuous hours on {day.day_idx}.")
                if total_day_hour_count != 0 and debug:
                    print(f"\t{worker.name} worked for {continuous_hour_message} continuous hours for {total_day_hour_count} total hours on {day.day_idx}.")
                score += fitness_total_hours_per_day(total_day_hour_count)
                total_week_hour_count += total_day_hour_count
            score += fitness_total_hours_per_week(total_week_hour_count, worker.base_hour_target)
        return  score

    def get_resting_days_score(self):
        pass

    @staticmethod
    def heritage(dna_one: DNA, dna_two: DNA) -> DNA:
        weeks = []
        for week_idx in range(0, dna_one.weeks_count):
            days = []
            days_append = days.append
            for day_idx in range(0, dna_one.weeks[week_idx].days_count):
                if day_idx % 2 == 0:
                    days_append(dna_one.weeks[week_idx].days[day_idx])
                else:
                    days_append(dna_two.weeks[week_idx].days[day_idx])
            weeks.append(GeneticWeek(days))
        dna = DNA.__new__(DNA)
        dna.weeks = weeks
        dna.built_at_gen = 0
        dna.valid_interday = False
        dna.valid_worker_time_per_day = False
        dna.origin = GenomeOrigin.INHERITANCE
        dna.real_origin = GenomeOrigin.INHERITANCE
        dna.weeks_count = len(dna.weeks)
        return dna

    @staticmethod
    def sort_adn_list_per_fitness(dnas: [DNA], generation: int) -> ([DNA], int):
        import config
        dnas = config.thread_pool.map(Fitness.calc_fitness, dnas)
        dnas.sort(key=lambda x: x.score, reverse=True)
        scores = [dna.score for dna in dnas]
        for i in range(0, 90, 10):
            p = numpy.percentile(scores, i)
            print(f"Percentile {i}th: {p}")
            Fitness.percentile[i] = {'percentile': i, 'value': p}
        for i in range(90, 100):
            p = numpy.percentile(scores, i)
            print(f"{base_tool.MUTATED_COLOR}Percentile {i}th: {p}{base_tool.NO_COLOR}")
            Fitness.percentile[i] = {'percentile': i, 'value': p}
        for i in range(990, 1000):
            p = numpy.percentile(scores, i/10)
            print(f"{base_tool.INHERITED_COLOR}Percentile {i/10}th: {p}{base_tool.NO_COLOR}")
            Fitness.percentile[i/10] = {'percentile': i/10, 'value': p}
        for i in range(0, 15):
            Fitness.best_dnas[i] = dnas[i]
        Fitness.generation = generation
        return dnas, dnas[0].score

