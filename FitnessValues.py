FITNESS_EMPTY_WORKER_SLOT = -50
FITNESS_UNDERHOUR_PER_WEEK = -3


def fitness_continuous_hours_values(hour, last=False):
    diff = abs(8-hour)
    switcher = {
        0: 0,
        1: -100,
        2: -200,
        3: -400,
        4: -600,
        5: -800,
        6: -1600,
        7: -2500,
        8: 0,
    }
    if last and hour == 1:
        print("One hour !")
    return switcher.get(diff, -3000)


def fitness_total_hours_per_day(hour):
    diff = abs(8 - hour)
    switcher = {
        0: 0,
        1: 0,
        2: -100,
        3: -200,
        4: -400,
        5: -800,
        6: -1600,
        7: -2500,
        8: 0,
    }
    return switcher.get(diff, -3000)


def fitness_total_hours_per_week(hour, base_worker_hour_target):
    difference = abs(base_worker_hour_target - hour)
    if difference < 3:
        return -50
    if difference < 6:
        return -200
    if difference < 9:
        return -800
    if difference < 12:
        return -2400
    if difference >= 12:
        return -10000
