import sys
from abc import ABC

from Workers import Workers, Worker


class GeneticDay(ABC):

    def __init__(self, day_idx):
        self.hours = []
        self.day_idx = day_idx
        self.hours_count = 0

    def hour_has_open_slot(self, hour_idx:int) -> bool:
        return True if None in self.hours[hour_idx] else False

    def hour_has_open_slot_for_worker(self, hour_idx:int, worker: Worker):
        if hour_idx >= self.hours_count:
            return -1
        result_idx = -1
        for i in range(0, len(self.hours[hour_idx])):
            if self.hours[hour_idx][i] is None and result_idx == -1:
                result_idx = i
            if self.hours[hour_idx][i] == worker.name:
                return -1
        return result_idx

    def recurs_set_worker(self, hour_start_idx: int, worker: Worker, min_continuous_hours: int, recursed: int = 0, max_continuous_hours: int = 9):
        if recursed == max_continuous_hours:
            return True
        if worker.remaining_hours < 1 and min_continuous_hours < 1:
            return True
        elif worker.remaining_hours < 1:
            return False

        slot_idx = self.hour_has_open_slot_for_worker(hour_start_idx, worker)
        if slot_idx != -1:
            self.hours[hour_start_idx][slot_idx] = worker.name
            worker.remaining_hours -= 1
            if not self.recurs_set_worker(hour_start_idx + 1, worker, min_continuous_hours - 1, recursed=recursed+1):
                # print(f"Reset hours slot {hour_start_idx:3} | {slot_idx} to None")
                self.hours[hour_start_idx][slot_idx] = None
                worker.remaining_hours += 1
                return False
            else:
                # print(f"{hour_start_idx:3} | {slot_idx} | {worker.name}")
                return True
        elif min_continuous_hours < 1:
            return True
        else:
            return False

    def has_any_hour_slot_remaining(self):
        for hour in range(0, self.hours_count):
            for slot in range(0, len(self.hours[hour])):
                if self.hours[hour][slot] is None:
                    # print(f"Empty slot is present {self.day_idx} | {hour:2} | {slot}")
                    return hour
        return None

    @staticmethod
    def worker_is_assigned_to_hour(worker: Worker, hour: {}):
        for slot in hour:
            if slot == worker.name:
                return True
        return False

    def copy(self):
        new_genetic_day = type(self)()
        new_genetic_day.day_idx = self.day_idx
        new_genetic_day.hours = [hour[:] for hour in self.hours]
        return new_genetic_day

    @staticmethod
    def get_hours_count_between_days(current_day_hour_idx:int, prev_day_hour_idx:int) -> int:
        if prev_day_hour_idx == -1:
            return sys.maxsize
        if current_day_hour_idx == -1:
            return sys.maxsize
        return current_day_hour_idx + 7 + (16 - prev_day_hour_idx)

    @staticmethod
    def get_first_hour_for_worker(day, worker: Worker):
        for hour_idx in range(0, day.hours_count):
            for slot in day.hours[hour_idx]:
                if slot == worker.name:
                    return hour_idx
        return -1

    @staticmethod
    def get_last_hour_for_worker(day, worker: Worker):
        for hour_idx, _ in reversed(list(enumerate(day.hours))):
            for slot in day.hours[hour_idx]:
                if slot == worker.name:
                    return hour_idx
        return -1

    def check_hour_count_per_worker(self, workers: Workers):
        dictionnary = {}
        for worker in workers.workers:
            dictionnary[worker.name] = 0
        for hour in self.hours:
            for slot in hour:
                if slot is not None:
                    dictionnary[slot] += 1
        for _, v in dictionnary.items():
            if v > 12:
                return False
        return True

