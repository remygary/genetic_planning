from Genome.Days.DayOfWeek import *
from Workers import Workers


class GeneticWeek:

    def __init__(self, days: []=None):
        self.workers = Workers()
        if days is None:
            self.days = []
            self.days.append(Monday())
            self.days.append(Tuesday())
            self.days.append(Wednesday())
            self.days.append(Thursday())
            self.days.append(Friday())
            self.days.append(Saturday())
            self.days.append(Sunday())
        else:
            self.days = days
            self.count_worker_remaining_hour_per_worker()
        self.days_count = len(self.days)

    def has_any_day_any_hour_slot_remaining(self):
        for day in self.days:
            open_hour = day.has_any_hour_slot_remaining()
            if open_hour is not None:
                return day, open_hour
        return None, None

    def count_worker_remaining_hour_per_worker(self):
        for worker in self.workers.workers:
            for day in self.days:
                for hour in day.hours:
                    for i in range(0, len(hour)):
                        if hour[i] == worker.name:
                            worker.remaining_hours -= 1

    def check_worker_evening_morning_rule(self, prev_week=None):
        for worker in self.workers.workers:
            prev_day = None
            if prev_week is not None:
                prev_day = prev_week.days[6]
            for day in self.days:
                if prev_day is None:
                    prev_day = day
                    continue
                current_day_first_hour = GeneticDay.get_first_hour_for_worker(day, worker)
                prev_day_last_hour = GeneticDay.get_last_hour_for_worker(prev_day, worker)
                # print(f"Worker {worker.name} ended last day at index {prev_day_last_hour} and start current day at index {current_day_first_hour}")
                count = GeneticDay.get_hours_count_between_days(
                    current_day_first_hour,
                    prev_day_last_hour)
                # print(f"Hour count between days: {count}")
                if count < 12:
                    return False
                prev_day = day
        return True

    def copy(self):
        new_week = GeneticWeek.__new__(GeneticWeek)
        new_week.workers = self.workers.copy()
        new_week.days = [day.copy() for day in self.days]
        new_week.days_count = len(new_week.days)
        return new_week


