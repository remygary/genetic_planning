from Genome.GeneticDay import GeneticDay


class Monday(GeneticDay):
    def __init__(self):
        super().__init__('Monday')
        self.hours = [
            [None],                 # 9h  -  0
            [None],                 # 10h -  1
            [None, None],           # 11h -  2
            [None, None],           # 12h -  3
            [None, None],           # 13h -  4
            [None, None],           # 14h -  5
            [None, None],           # 15h -  6
            [None, None],           # 16h -  7
            [None, None],           # 17h -  8
            [None, None],           # 18h -  9
            [None, None],           # 19h - 10
            [None, None, None],     # 20h - 11
            [None, None, None],     # 21h - 12
            [None, None, None],     # 22h - 13
            [None, None],           # 23h - 14
            [None, None],           # 24h - 15
            [None, None],           # 01h - 16
        ]
        self.hours_count = len(self.hours)


class Tuesday(GeneticDay):
    def __init__(self):
        super().__init__('Tuesday')
        self.hours = [
            [None],  # 9h
            [None],  # 10h
            [None, None],  # 11h
            [None, None],  # 12h
            [None, None],  # 13h
            [None, None],  # 14h
            [None, None],  # 15h
            [None, None],  # 16h
            [None, None],  # 17h
            [None, None],  # 18h
            [None, None],  # 19h
            [None, None],  # 20h
            [None, None],  # 21h
            [None, None],  # 22h
            [None, None],  # 23h
            [None, None],  # 24h
            [None, None],  # 01h
        ]
        self.hours_count = len(self.hours)


class Wednesday(GeneticDay):
    def __init__(self):
        super().__init__('Wednesday')
        self.hours = [
            [None],  # 9h
            [None],  # 10h
            [None, None],  # 11h
            [None, None, None],  # 12h
            [None, None, None],  # 13h
            [None, None, None],  # 14h
            [None, None],  # 15h
            [None, None],  # 16h
            [None, None],  # 17h
            [None, None],  # 18h
            [None, None],  # 19h
            [None, None],  # 20h
            [None, None],  # 21h
            [None, None],  # 22h
            [None, None],  # 23h
            [None, None],  # 24h
            [None, None],  # 01h
        ]
        self.hours_count = len(self.hours)


class Thursday(GeneticDay):
    def __init__(self):
        super().__init__('Thursday')
        self.hours = [
            [None],  # 9h
            [None],  # 10h
            [None, None],  # 11h
            [None, None],  # 12h
            [None, None],  # 13h
            [None, None],  # 14h
            [None, None],  # 15h
            [None, None],  # 16h
            [None, None],  # 17h
            [None, None, None],  # 18h
            [None, None, None],  # 19h
            [None, None, None],  # 20h
            [None, None, None],  # 21h
            [None, None, None],  # 22h
            [None, None, None],  # 23h
            [None, None],  # 24h
            [None, None],  # 01h
        ]
        self.hours_count = len(self.hours)


class Friday(GeneticDay):
    def __init__(self):
        super().__init__('Friday')
        self.hours = [
            [None],  # 9h
            [None],  # 10h
            [None, None],  # 11h
            [None, None],  # 12h
            [None, None],  # 13h
            [None, None],  # 14h
            [None, None],  # 15h
            [None, None],  # 16h
            [None, None],  # 17h
            [None, None, None],  # 18h
            [None, None, None],  # 19h
            [None, None, None],  # 20h
            [None, None, None],  # 21h
            [None, None, None],  # 22h
            [None, None, None],  # 23h
            [None, None],  # 24h
            [None, None],  # 01h
        ]
        self.hours_count = len(self.hours)


class Saturday(GeneticDay):
    def __init__(self):
        super().__init__('Saturday')
        self.hours = [
            [None],                     # 9h
            [None, None],               # 10h
            [None, None, None],         # 11h
            [None, None, None, None],   # 12h
            [None, None, None, None],   # 13h
            [None, None, None, None],   # 14h
            [None, None, None, None],   # 15h
            [None, None, None, None],   # 16h
            [None, None, None, None],   # 17h
            [None, None, None, None],   # 18h
            [None, None, None, None],   # 19h
            [None, None, None, None],   # 20h
            [None, None, None, None],   # 21h
            [None, None, None, None],   # 22h
            [None, None, None],         # 23h
            [None, None, None],         # 24h
            [None, None, None],         # 01h
        ]
        self.hours_count = len(self.hours)


class Sunday(GeneticDay):
    def __init__(self):
        super().__init__('Sunday')
        self.hours = [
            [None],                     # 9h
            [None, None],               # 10h
            [None, None, None],         # 11h
            [None, None, None, None],   # 12h
            [None, None, None, None],   # 13h
            [None, None, None, None],   # 14h
            [None, None, None, None],   # 15h
            [None, None, None, None],   # 16h
            [None, None, None, None],   # 17h
            [None, None, None],         # 18h
            [None, None, None],         # 19h
            [None, None, None],         # 20h
            [None, None, None],         # 21h
            [None, None, None],         # 22h
            [None, None],               # 23h
            [None, None],               # 24h
            [None, None],               # 01h
        ]
        self.hours_count = len(self.hours)
