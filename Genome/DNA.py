import copy
import hashlib

from Genome.GeneticWeek import GeneticWeek
from Tools.genome_origin import GenomeOrigin


class DNA:

    def __init__(self, idx: int=-1):
        self.idx = idx
        self.weeks = []
        self.score = 0
        self.origin = GenomeOrigin.RE_GENERATION
        self.real_origin = GenomeOrigin.RE_GENERATION
        self.built_at_gen = 0
        self.valid_interday = False
        self.valid_worker_time_per_day = False
        self.representation = 0
        for i in range(0, 1):
            self.weeks.append(GeneticWeek())
        self.weeks_count = len(self.weeks)

    def __hash__(self):
        return self.representation

    def __eq__(self, other):
        return self.representation == other.representation

    @staticmethod
    def build_string_representation(dna):
        representation = []
        for week_idx in range(0, dna.weeks_count):
            week = dna.weeks[week_idx]
            for days_idx in range(0, week.days_count):
                day = week.days[days_idx]
                for hour_idx in range(0, day.hours_count):
                    hour = day.hours[hour_idx]
                    for slot in hour:
                        if slot is None:
                            representation.append('none')
                        else:
                            representation.append(slot)
        representation = ''.join(representation)
        dna.representation = int(hashlib.md5(representation.encode('utf-8')).hexdigest(), 16)

    @staticmethod
    def build_string_representation2(dna):
        representation = []
        representation_append = representation.append
        for week in dna.weeks:
            for day in week.days:
                for hour in day.hours:
                    for slot in hour:
                        if slot is None:
                            representation_append('none')
                        else:
                            representation_append(slot)
        representation = ''.join(representation)
        dna.representation = int(hashlib.md5(representation.encode('utf-8')).hexdigest(), 16)


    @staticmethod
    def check_if_viable_interday(dna):
        prev_week = None
        for week in dna.weeks:
            if not week.check_worker_evening_morning_rule(prev_week):
                return dna
            prev_week = week
        dna.valid_interday = True
        return dna

    @staticmethod
    def check_if_viable_for_work_time_per_day(dna):
        for week in dna.weeks:
            for day in week.days:
                if not day.check_hour_count_per_worker(week.workers):
                    return dna
        dna.valid_worker_time_per_day = True
        return dna

