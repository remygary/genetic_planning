import itertools


from flask import Flask, json
from Main import Main
from Fitness import Fitness

app = Flask(__name__)


def flaskThread():
    app.run()


@app.route('/best_result')
def get_best_result():
    best_dnas = Fitness.best_dnas
    data = []
    i = 0
    data.append({"Generation": Fitness.generation})
    for _, dna in best_dnas.items():
        weeks = {}
        for week in dna.weeks:
            days = {}
            for day in week.days:
                days[day.day_idx] = day.hours
            weeks[i] = days
            i += 1
        data.append({
            "score": dna.score,
            "origin": str(dna.origin)[13:],
            "real_origin": str(dna.real_origin)[13:],
            "built_at_gen": dna.built_at_gen,
            "valid_interday": dna.valid_interday,
            "valid_worker_time_per_day": dna.valid_worker_time_per_day,
            "plan": weeks
        })
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json',
        headers={'Access-Control-Allow-Origin': '*'}
    )
    return response

@app.route('/percentile')
def get_percentile():
    data = Fitness.percentile
    data[-1] = {'generation': Fitness.generation}
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json',
        headers={'Access-Control-Allow-Origin': '*'}
    )
    return response


