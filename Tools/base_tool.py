from Genome.DNA import DNA
from Tools.genome_origin import GenomeOrigin

PREVIOUS_BEST_COLOR = '\033[91m'
MUTATED_COLOR = '\033[94m'
INHERITED_COLOR = '\033[92m'
GENERATED_COLOR = '\033[96m'
NO_COLOR = '\033[0m'


def print_best_fitness(generation: int, best_fitness: int, dna: DNA):
    if dna.origin == GenomeOrigin.PREV_BEST:
        color_to_use = PREVIOUS_BEST_COLOR
    elif dna.origin == GenomeOrigin.RE_GENERATION:
        color_to_use = MUTATED_COLOR
    elif dna.origin == GenomeOrigin.MUTATION:
        color_to_use = INHERITED_COLOR
    elif dna.origin == GenomeOrigin.INHERITANCE:
        color_to_use = GENERATED_COLOR
    print(
        f"Generation {generation}: New Best fitness is: {best_fitness} from {color_to_use}{dna.origin}{NO_COLOR}")