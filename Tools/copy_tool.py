from Genome.DNA import DNA


class CopyTool:
    def __init__(self):
        pass

    @staticmethod
    def copy_dna(dna: DNA):
        new_dna = DNA.__new__(DNA)
        new_dna.weeks = [week.copy() for week in dna.weeks]
        new_dna.built_at_gen = 0
        new_dna.valid_interday = False
        new_dna.valid_worker_time_per_day = False
        new_dna.representation = 0
        new_dna.weeks_count = len(new_dna.weeks)
        return new_dna
