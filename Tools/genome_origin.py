from enum import Enum


class GenomeOrigin(Enum):
    PREV_BEST = 'PREV_BEST'
    MUTATION = 'MUTATION'
    RE_GENERATION = 'RE_GENERATION'
    INHERITANCE = 'INHERITANCE'
