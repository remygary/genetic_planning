from random import Random

from DNAGenerator import DNAGenerator
from DNARandomizer import DNARandomizer
from Fitness import Fitness
from Genome.DNA import DNA
from Tools.copy_tool import CopyTool
from Tools.genome_origin import GenomeOrigin


class LifeCycle:

    rand = Random()

    @staticmethod
    def mass_generation(count: int) -> [DNA]:
        import config
        dnas = [DNA(i) for i in range(0, count)]
        dnas = list(config.thread_pool.map(DNAGenerator.generate, dnas))
        return dnas

    @staticmethod
    def mass_mutate(dnas: [DNA], result_list_size: int, mutation_rate: int) -> [DNA]:
        import config
        """Take a list of dna, mutate each X times to product a 'result_list_size' sized list of mutated dna"""
        dnas_copy = []
        while len(dnas_copy) < result_list_size:
            tmp_dnas = list(config.thread_pool.map(CopyTool.copy_dna, dnas))
            tmp_dnas = [DNARandomizer.randomize(copy, int(mutation_rate)) for copy in tmp_dnas]
            dnas_copy.extend(tmp_dnas)
        dnas_copy = dnas_copy[:int(result_list_size)]
        return dnas_copy

    @staticmethod
    def inheritance(dnas_one: [DNA], dnas_two: [DNA]) -> [DNA]:
        dnas = []
        dnas_append = dnas.append
        dnas_one_len = len(dnas_one)
        dnas_two_len = len(dnas_two)
        for i in range(0, min(dnas_one_len, dnas_two_len)):
            val_one = LifeCycle.rand.randint(0, dnas_one_len - 1)
            val_two = LifeCycle.rand.randint(0, dnas_two_len - 1)
            dnas_append(Fitness.heritage(dnas_one[val_one], dnas_two[val_two]))
        return dnas

    @staticmethod
    def life_cycle(dnas: [DNA], generation: int) -> [DNA]:
        import config
        len_dnas = len(dnas)
        tenth_len_dans = int(len_dnas / 10)
        splitted_dna = [dnas[x:x + tenth_len_dans] for x in range(0, len_dnas, tenth_len_dans)]
        new_dnas = [[] for x in range(0, 10)]

        """REBUILD"""
        new_dnas[0] = splitted_dna[0]
        for dna in new_dnas[0]:
            dna.origin = GenomeOrigin.PREV_BEST

        """INHERITANCE"""
        rand = Random()
        rand_list = rand.sample(range(4), 4)
        new_dnas[5] = LifeCycle.inheritance(splitted_dna[rand_list[0]], splitted_dna[rand_list[1]])
        new_dnas[6] = LifeCycle.inheritance(splitted_dna[rand_list[2]], splitted_dna[rand_list[3]])

        """MUTATION"""
        new_dnas[1] = LifeCycle.mass_mutate(splitted_dna[0] , tenth_len_dans, 5)
        new_dnas[2] = LifeCycle.mass_mutate(splitted_dna[1] , tenth_len_dans, 5)
        new_dnas[3] = LifeCycle.mass_mutate(new_dnas[5]     , tenth_len_dans, 5)
        new_dnas[4] = LifeCycle.mass_mutate(new_dnas[6]     , tenth_len_dans, 5)

        """INHERITANCE AGAIN"""
        new_dnas[7] = LifeCycle.inheritance(new_dnas[1], new_dnas[3])
        new_dnas[8] = LifeCycle.inheritance(new_dnas[2], new_dnas[4])
        new_dnas[9] = LifeCycle.inheritance(splitted_dna[0], splitted_dna[0])

        """REBUILD SINGLE LIST"""
        new_dnas = [item for sublist in new_dnas for item in sublist]

        """CHECK DNA VIABILITY"""
        new_dnas = list(config.thread_pool.map(DNA.check_if_viable_interday, new_dnas))
        new_dnas = list(config.thread_pool.map(DNA.check_if_viable_for_work_time_per_day, new_dnas))

        """REMOVE DUPLICATES"""
        [DNA.build_string_representation2(dna) for dna in new_dnas]
        new_dnas = list(set(new_dnas))

        """REGENERATION"""
        new_dnas.extend(LifeCycle.mass_generation(len_dnas - len(new_dnas)))

        for dna in new_dnas:
            if dna.built_at_gen == 0:
                dna.built_at_gen = generation
        return new_dnas
